# Node Exporter Role


 Provision Prometheus Node Exporter on a static node.

## Requirements:
**Minimum Ansible version**: `2.9`
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*CentOS*|**7,8**|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Install:
* Check if Node Exporter package is already installed 
* Download Node Exporter package 
* Unpack Node Exporter package 
* Set permissions of the package directory 
* Create a symlink to Node Exporter package directory 
#### Systemd:
* Copy Node Exporter service file 
#### Prepare:
* Create Node Exporter system group 
* Create Node Exporter system user 
* Create Node Exporter directories 
* Create symbolik links to Node Exporter directories 
#### Configure:
* Copy Node Exporter configuration files 
#### Handlers:
* Restart Node Exporter - Enable + restart Node Exporter service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`nodeexporter_version`|`'1.6.1'`|Node Exporter version to install (Supported versions: 1.6.1, 1.4.0).|
|`nodeexporter_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
|`nodeexporter_args`|`[]`|List of arguments to pass to Node Exporter during the startup. Example: ['--path.procfs="/proc"', '--collector.ntp.ip-ttl=1'].|
|`nodeexporter_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`nodeexporter_installation_dir`|`'/opt/prometheus'`|Temporary storage path.|
|`nodeexporter_data_dir`|`"{{ nodeexporter_installation_dir }}/data"`|Application data directory|
|`nodeexporter_temp_dir`|`'/tmp/nodeexporter'`|Temporary storage path.|
|`nodeexporter_logs_dir`|`"{{ nodeexporter_logs_dir }}/logs"`|Application logs directory.|
|`nodeexporter_user`|`'nodeexporter'`|Prometheus system user.|
|`nodeexporter_group`|`'nodeexporter'`|Prometheus system group.|
## Tags:

* `nodeexporter.install` - Execute all installation tasks.


* `nodeexporter.configure` - Execute tasks related to Node Exporter configuration.


* `nodeexporter.install.prepare.user` - Create Node Exporter user and group.


* `nodeexporter.install.prepare.directories` - Create Node Exporter directories.


* `nodeexporter.install.prepare` - Prepare the environment for Node Exporter installation


* `nodeexporter.install.package` - Install Node Exporter package


* `nodeexporter.install.service` - Run Node Exporter service setup


* `nodeexporter.configure.files` - Synchronize Node Exporter settings files

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.prometheus.nodeexporter'

```

## Author Information
This role  was created by: *BOBO*
