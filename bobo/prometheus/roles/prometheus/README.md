# Prometheus Role


 Provision Prometheus on a static node.

## Requirements:
**Minimum Ansible version**: `2.9`
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*CentOS*|**7,8**|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Install:
* Check if Prometheus package is already installed 
* Download Prometheus package 
* Unpack Prometheus package 
* Set permissions of the package directory 
* Create a symlink to Prometheus package directory 
#### Systemd:
* Copy Prometheus service file 
#### Prepare:
* Create Prometheus system group 
* Create Prometheus system user 
* Create Prometheus directories 
* Create symbolik links to Prometheus directories 
#### Configure:
* Copy Prometheus configuration files 
#### Handlers:
* Restart Prometheus - Enable + restart Prometheus service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`prometheus_version`|`'2.47.1'`|Prometheus version to install (Supported versions: 2.47.1, 2.39.0).|
|`prometheus_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
|`prometheus_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`prometheus_installation_dir`|`'/opt/prometheus'`|Temporary storage path.|
|`prometheus_data_dir`|`"{{ prometheus_installation_dir }}/data"`|Application data directory|
|`prometheus_temp_dir`|`'/tmp/prometheus'`|Temporary storage path.|
|`prometheus_logs_dir`|`"{{ prometheus_logs_dir }}/logs"`|Application logs directory.|
|`prometheus_user`|`'prometheus'`|Prometheus system user.|
|`prometheus_group`|`'prometheus'`|Prometheus system group.|
## Tags:

* `prometheus.install` - Execute all installation tasks.


* `prometheus.configure` - Execute tasks related to Prometheus configuration.


* `prometheus.install.prepare.user` - Create Prometheus user and group.


* `prometheus.install.prepare.directories` - Create Prometheus directories.


* `prometheus.install.prepare` - Prepare the environment for Prometheus installation


* `prometheus.install.package` - Install Prometheus package


* `prometheus.install.service` - Run Prometheus service setup


* `prometheus.configure.files` - Synchronize Prometheus settings files

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.prometheus.prometheus'

```

## Author Information
This role  was created by: *BOBO*
