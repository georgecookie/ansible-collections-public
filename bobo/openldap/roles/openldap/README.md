# openldap

 OpenLDAP server for Linux.

## Requirements:
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Configure:
* Copy OpenLDAP configuration files 
* Copy OpenLDAP TLS files 
#### Install:
* Ensure OpenLDAP packages are installed. 
* Set DN suffix. 
* Set admin DN. 
* Convert admin password to a hash. 
* Set admin password after the fresh installation. 
* Set TLS settings. 
#### Handlers:
* Restart OpenLDAP - Enable + restart OpenLDAP service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`openldap_version`|`'2.5'`|OpenLDAP version to install (Supported versions: 2.5).|
|`openldap_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
|`openldap_tls_files`|`{}`|Dictionary of tls files. FORMAT: {'ca': CONTENT, 'cert': CONTENT, 'key': CONTENT}|
|`openldap_listen_urls`|`['ldap:///', 'ldapi:///']`|OpenLDAP is normally served only on all TCP-ports 389. OpenLDAP can also be served on TCP-port 636 (ldaps) and via unix sockets. Example urls: ldap://127.0.0.1:389/, ldaps:///, ldapi:///|
|`openldap_args`|`[]`|Extra arguments passed to slapd daemon.|
|`openldap_dn_suffix`|`'dc=example,dc=com'`|DN suffix. Only set when OpenLDAP package is being installed.|
|`openldap_admin_password`|`'cn=admin,dc=ldap,dc=bobo'`|RootDN admin user entry. Only set when OpenLDAP package is being installed.|
|`openldap_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
## Tags:

* `openldap.install` - Run the installation tasks.


* `openldap.configure` - Run the configuration tasks.

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.openldap.openldap'

```

