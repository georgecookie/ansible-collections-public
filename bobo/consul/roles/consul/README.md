# Consul Role


 Provision Consul on a static node.

## Requirements:
**Minimum Ansible version**: `2.9`
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*CentOS*|**7,8**|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Install:
* Check if Consul package is already installed 
* Download Consul package 
* Unpack Consul package 
#### Systemd:
* Copy Consul service file 
#### Prepare:
* Create Consul system group 
* Create Consul system user 
* Create Consul directories 
* Create symbolik links to Consul directories 
#### Configure:
* Copy Consul configuration files 
#### Handlers:
* Restart Consul - Enable + restart Consul service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`consul_version`|`'1.13.2'`|Consul version to install (Supported versions: 1.13.2).|
|`consul_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
|`consul_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`consul_installation_dir`|`'/opt/consul'`|Temporary storage path.|
|`consul_data_dir`|`"{{ consul_installation_dir }}/data"`|Application data directory|
|`consul_config_dir`|`"{{ consul_config_dir }}/consul/config"`|Application configuration directory|
|`consul_temp_dir`|`'/tmp/consul'`|Temporary storage path.|
|`consul_logs_dir`|`"{{ consul_logs_dir }}/logs"`|Application logs directory.|
|`consul_user`|`'consul'`|Consul system user.|
|`consul_group`|`'consul'`|Consul system group.|
## Tags:

* `consul.install` - Execute all installation tasks.


* `consul.configure` - Execute tasks related to Consul configuration.


* `consul.install.prepare.user` - Create Consul user and group.


* `consul.install.prepare.directories` - Create Consul directories.


* `consul.install.prepare` - Prepare the environment for Consul installation


* `consul.install.package` - Install Consul package


* `consul.install.service` - Run Consul service setup


* `consul.configure.files` - Synchronize Consul settings files

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.consul.consul'

```

## Author Information
This role  was created by: *BOBO*
