# postgresql

 PostgreSQL server for Linux.

## Requirements:
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Install:
* Ensure PostgreSQL Python libraries are installed. 
* Ensure PostgreSQL packages are installed. 
* Set PostgreSQL environment variables. 
* Ensure PostgreSQL data directory exists. 
* Ensure PostgreSQL unix socket dirs exist. 
* Ensure PostgreSQL database is initialized. 
* Ensure PostgreSQL log directory exists. 
#### Configure:
* Copy PostgreSQL configuration files 
#### Handlers:
* Restart Consul - Enable + restart PostgreSQL service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`postgres_version`|`'14'`|PostgreSQL version to install (Supported versions: 14).|
|`postgres_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
|`postgres_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`postgres_data_dir`|`"/var/lib/postgresql/{{ postgresql_version }}/main"`|Application data directory|
|`postgres_logs_dir`|`"{{ postgres_data_dir }}/log"`|PostgreSQL logs directory. Only used if logging_collector is on.|
|`postgres_unix_socket_dirs`|`['/var/run/postgresql']`|PostgreSQL unix socket directories to create|
|`postgres_user`|`'postgres'`|PostgreSQL system user|
|`postgres_group`|`'postgres'`|PostgreSQL system group|
## Tags:

* `postgres.install` - Execute all installation tasks.


* `postgres.configure` - Execute tasks related to PostgreSQL configuration.

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.postgresql.postgresql'

```

