# Harbor Role


 Provision Harbor on a static node.

## Requirements:
**Minimum Ansible version**: `2.9`
## Actions:

Actions performed by this role


#### General:
* Set runtime facts 
#### Installation:
* Create the filesystem structure, download and execute the installation package 
#### Configuration:
* Run the application's configuration tasks 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`harbor_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`harbor_installation_dir`|`'/opt/harbor'`|Temporary storage path.|
|`harbor_temp_dir`|`'/tmp/harbor'`|Temporary storage path.|
|`harbor_data_dir`|`"{{ harbor_installation_dir }}/data"`|Application data directory|
|`harbor_logs_dir`|`"{{ harbor_logs_dir }}/logs"`|Application logs directory.|
|`harbor_version`|`'2.9.0'`|Harbor version to install (Supported versions: 2.9.0).|
|`harbor_installation_script_args`|`[]`|Harbor install.sh script arguments to add.|
|`harbor_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
## Tags:

* `harbor.install` - Install Harbor.


* `harbor.configure` - Configure Harbor.

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.harbor.harbor'

```

## Author Information
This role  was created by: *BOBO*
