# Bobo Jenkins Server Role


 The role installs and provisions a Jenkins server.

## Requirements:
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### Prepare:
* Check that Jenkins version is set correctly 
* Find the OS distribution name and main version 
#### Install:
* Install Jenkins prerequisites 
* Create Jenkins group 
* Create Jenkins user 
* Create jenkins installation directory 
* Create jenkins installation directory for binary download 
* Copy Jenkins service files 
* Check if Jenkins package is already installed 
* Download Jenkins package 
* Unpack Jenkins package 
* Create a symlink to Jenkins package directory 
* Create jenkins directories 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`jenkins_version`|`'2.414.1'`|Kafka version to install. Supported versions: '2.414.1'|
|`jenkins_args`|`["--httpListenAddress=127.0.0.1", "--httpPort=8080", "--webroot='%C/jenkins/war'"]`|List of arguments to pass to Jenkins binary during the startup.|
|`jenkins_configure_only`|`no`|Run tasks but do not stop/restart currently running services.|
|`jenkins_java_options`|`['-Djava.awt.headless=true']`|List of Java options to set when starting Jenkins server.|
|`jenkins_java_bin_path`|`'/usr/bin/java'`|Path to Java binary.|
|`jenkins_user`|`'jenkins'`|Jenkins user that the process will run under.|
|`jenkins_group`|`'jenkins'`|Jenkins group that the process will run under.|
|`jenkins_installation_dir`|`'/opt/jenkins'`|Jenkins installation directory. NOTE: If the directory exists, the owner and permissions might be changed.|
|`jenkins_data_dir`|`"{{ jenkins_installation_dir }}/jenkins/data/jenkins"`|Application data directory. NOTE: If the directory exists, the owner and permissions might be changed.|
|`jenkins_logs_dir`|`"{{ jenkins_data_dir }}/logs"`|Application log directory. NOTE: If the directory exists, the owner and permissions might be changed. By default, Jenkins uses systemd-journald. In order to write logs to a file, option --logfile must be specified.|
|`jenkins_temp_dir`|`'/tmp/jenkins'`|Temporary storage path. NOTE: If the directory exists, the owner and permissions might be changed.|
## Tags:

* `jenkins.install` - Install Jenkins package.


* `kafka.install.prepare` - Prepare the hosts for installation of software packages.


* `kafka.install.package` - Install software packages.


* `kafka.install.service` - Configure systemd services.


* `jenkins.install.prepare.prerequisites` - Install dependecies required to run Jenkins.


* `jenkins.install.prepare.user` - Create Jenkins user and group.


* `jenkins.install.prepare.dirs` - Create Jenkins installation directory.


* `jenkins.install.package.app_dirs` - Create Jenkins application directories.


* `kafka.install.package.app_dirs` - 

## Examples:

```yaml
# Example 1
- hosts: test-host
  gather_facts: yes
  become: yes
  roles:
    - 'bobo.jenkins.server'

```

## Author Information
This role  was created by: *bobo*
