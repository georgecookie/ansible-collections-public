# Grafana Role


 Provision Grafana on a static node.

## Requirements:
**Minimum Ansible version**: `2.9`
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*CentOS*|**7,8**|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Install:
* Check if Grafana package is already installed 
* Download Grafana package 
* Unpack Grafana package 
* Set permissions of the package directory 
* Create a symlink to Grafana package directory 
* Create Grafana provisioning directory 
* Create Grafana plugins directory 
#### Systemd:
* Copy Grafana service file 
#### Prepare:
* Create Grafana system group 
* Create Grafana system user 
* Create Grafana directories 
* Create symbolik links to Grafana directories 
#### Configure:
* Copy Grafana configuration files 
* Check if Grafana configuration file exists 
* Generate Grafana initial configuration file 
* Create Grafana provisioning directories 
* Copy exported dashboard definitions 
* Copy Grafana access-control provisioning files 
* Copy Grafana alerting provisioning files 
* Copy Grafana dashboards provisioning files 
* Copy Grafana datasources provisioning files 
* Copy Grafana notifiers provisioning files 
* Copy Grafana plugins provisioning files 
#### Handlers:
* Restart Grafana - Enable + restart Grafana service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`grafana_version`|`'10.1.4'`|Grafana version to install (Supported versions: 10.1.4, 9.2.1).|
|`grafana_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value). NOTE that Grafana configuration file is set to 'grafana.ini'|
|`grafana_config_file_path`|`"{{ grafana_installation_dir }}/grafana/conf/defaults.ini"`|Grafana configuration file to load when starting the server.|
|`grafana_plugins_dir`|`"{{ grafana_installation_dir }}/plugins"`|The path to the directory with Grafana plugins.|
|`grafana_provisioning_dir`|`"{{ grafana_installation_dir }}/grafana/provisioning"`|The path to Grafana provisioning directory.|
|`grafana_provisioning_files`|`{}`|Grafana provisioning directory structure. Top level keys represent the resource to be provisioned (i.e. alerting, datasources, plugins, etc). Each top level key must point to at least one key-pair with the same structure as in grafana_configuration_files.|
|`grafana_dashboard_dirs`|`[]`|A list of paths on the ansible controller to directories that contain Grafana dashboard definitions in JSON format. These directories will be copied to "{{ grafana_data_dir }}/dashboards" directory on the remote host and can be referenced in the provisioning files. NOTE that if the path ends with '/', only the contents of the directory will be copied.|
|`grafana_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`grafana_installation_dir`|`'/opt/grafana'`|Temporary storage path.|
|`grafana_data_dir`|`"{{ grafana_installation_dir }}/data"`|Application data directory|
|`grafana_temp_dir`|`'/tmp/grafana'`|Temporary storage path.|
|`grafana_logs_dir`|`"{{ grafana_logs_dir }}/logs"`|Application logs directory.|
|`grafana_user`|`'grafana'`|Prometheus system user.|
|`grafana_group`|`'grafana'`|Grafana system group.|
## Tags:

* `grafana.install` - Execute all installation tasks.


* `grafana.configure` - Execute tasks related to Grafana configuration.


* `grafana.install.prepare.user` - Create Grafana user and group.


* `grafana.install.prepare.directories` - Create Grafana directories.


* `grafana.install.prepare` - Prepare the environment for Grafana installation


* `grafana.install.package` - Install Grafana package


* `grafana.install.service` - Run Grafana service setup


* `grafana.configure.files` - Synchronize Grafana settings files


* `grafana.configure.provisioning` - Copy Grafana provisioning settings

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.grafana.grafana'

```

## Author Information
This role  was created by: *BOBO*
