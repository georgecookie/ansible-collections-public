# nginx

 NGINX server for Linux.

## Requirements:
#### Supported platforms:
|**Platform**|**Release**|
|---|---|
|*Ubuntu*|**22.04**|
## Actions:

Actions performed by this role


#### General:
* Find the OS distribution name and main version 
#### Configure:
* Copy NGINX configuration file 
* Copy NGINX sites configuration 
* Copy TLS files 
#### Install:
* Ensure NGINX packages are installed. 
* Create NGINX directories. 
#### Handlers:
* Reload NGINX - Reload NGINX 
* Restart NGINX - Enable + restart NGINX service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`nginx_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`nginx_version`|`'1.18'`|NGINX version to install (Supported versions: 1.18).|
|`nginx_config_file`|`default`|NGINX configuration file contents. Defaults to system package's default.|
|`nginx_sites_files`|`{}`|NGINX Sites configuration files. NOTE that the file names must have .conf extenstion FORMAT: {'FILE_NAME.conf': CONTENT}.|
|`nginx_tls_certs_dir`|`'/etc/nginx/tls'`|The path to the directory where TLS certificates will be saved.|
|`nginx_tls_files`|`{}`|Dictionary of tls files. This is an optional setting in case some NGINX sites require a TLS configuration. FORMAT: {'FILE_NAME': CONTENT}|
## Tags:

* `nginx.install` - Run the installation tasks.


* `nginx.configure` - Run the configuration tasks.

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.nginx.nginx'

```

