# Docker Role


 Provision Docker on a static node.

## Requirements:
**Minimum Ansible version**: `2.9`
## Actions:

Actions performed by this role


#### Install:
* Download Docker package 
* Unpack Docker package 
* Copy Docker binaries to the directory in PATH 
* Download Docker plugins 
* Create symlinks to Docker plugins 
* Create docker plugins directories 
#### Systemd:
* Copy Docker service files 
#### Prepare:
* Create Docker system group 
* Create temporary directory 
* Create Docker configuration directories 
#### Configure:
* Copy Docker configuration files 
#### Handlers:
* Restart Docker - Enable + restart Docker service 

## Variables:
|**Name**|**Default**|**Description**|
|---|---|---|
|`docker_configure_only`|`no`|Only configure the service, do not interact with already running processes.|
|`docker_version`|`'24.0.6'`|Docker version to install (Supported versions: 24.0.6).|
|`docker_plugins`|`{}`|Docker plugins to install. Format: {'NAME': 'VERSION'}. Supported plugins: {'compose': '2.20.3', 'buildx': '0.11.2'}|
|`docker_configuration_files`|`{}`|Dictionary of configuration file names (key) and their content (value).|
|`docker_tls_sites`|`[]`|List of Docker TLS certificates. Format: [{'site': 'example.com:port', 'cert': CONTENT, 'key': CONTENT, 'ca': CONTENT}, ...]|
|`docker_args`|`[]`|List of arguments to pass to Docker daemon during the startup.|
|`docker_plugins_dir`|`'/opt/docker_plugins'`|The directory where Docker plugins should be installed.|
|`docker_temp_dir`|`'/tmp/docker'`|Temporary storage path.|
|`docker_group`|`'docker'`|Docker system group.|
## Tags:

* `docker.install` - Execute all installation tasks.


* `docker.install.package.plugins` - Install Docker plugins


* `docker.install.prepare.user` - Create Docker user and group


* `docker.install.prepare.dirs` - Create temporary directory


* `docker.install.prepare` - Prepare the environment for Docker installation


* `docker.install.package` - Install Docker package


* `docker.install.service` - Run Docker setup


* `docker.configure.files` - Synchronize Docker settings files

## Examples:

```yaml
# Example
- hosts: test-host
  become: yes
  roles:
    - 'bobo.docker.docker'
  vars:
    docker_plugins:
      compose: '2.20.3'

```

## Author Information
This role  was created by: *BOBO*
