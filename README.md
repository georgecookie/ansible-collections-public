ansible-collections
====================

## Description
The repository includes a list of namespaces with various Ansible collections.

## Installation
To install all collections in ONE specific namespace (in this case `bobo`), use the following command:

```sh
ansible-galaxy collection install 'git@gitlab.com:georgecookie/ansible-collections#/bobo'
```

Alternatively, all required dependencies for an Ansible project can be specified in `requirements.yaml` file:

```yaml
---

collections:
  - 'git@gitlab.com:georgecookie/ansible-collections#/bobo'
```

Install all dependencies in `requirements.yaml` with the following command:

```sh
ansible-galaxy collection install -r requirements.yaml
```

